/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  testPathIgnorePatterns: ["<rootDir>/.next", "<rootDir>/node_modules"],
  moduleNameMapper: {
    "\\.(css)$": "<rootDir>/node_modules/jest-css-modules",
    "^axios$": require.resolve("axios")
  },
  transform: {
    "^.+\\.tsx?$": "babel-jest"
  },
  setupFiles: ["<rootDir>/setupJest.ts"]
};
