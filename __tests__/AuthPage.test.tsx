import "@testing-library/jest-dom/extend-expect";
import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { getPage, initTestHelpers } from "next-page-tester";

initTestHelpers();

process.env.NEXT_PUBLIC_RESTAPI_URL = "http://127.0.0.1:8000/api";
const url = String(process.env.NEXT_PUBLIC_RESTAPI_URL);

const handlers = [
  rest.post(`${url}/jwt/create/`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({ access: "123xyz" }));
  }),
  rest.post(`${url}/register/`, (req, res, ctx) => {
    return res(ctx.status(201));
  }),
  rest.get(`${url}/get-blogs/`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([
        {
          id: 1,
          title: "title1",
          content: "content1",
          username: "username1",
          tags: [
            { id: 1, name: "tag1" },
            { id: 2, name: "tag2" }
          ],
          created_at: "2021-01-12 14:59:41"
        },
        {
          id: 2,
          title: "title2",
          content: "content2",
          username: "username2",
          tags: [
            { id: 1, name: "tag1" },
            { id: 2, name: "tag2" }
          ],
          created_at: "2021-01-13 14:59:41"
        }
      ])
    );
  })
];
const server = setupServer(...handlers);
beforeAll(() => {
  server.listen();
});
afterEach(() => {
  server.resetHandlers();
  cleanup();
});
afterAll(() => {
  server.close();
});
describe("AdminPage Test Cases", () => {
  it("Should route to index-page when login succeeded", async () => {
    const { page } = await getPage({
      route: "/admin-page"
    });
    render(page);
    expect(await screen.findByText("Login")).toBeInTheDocument();
    await userEvent.type(screen.getByPlaceholderText("Username"), "user1");
    await userEvent.type(screen.getByPlaceholderText("Password"), "dummypw");
    await userEvent.click(screen.getByText("Login with JWT"));
    expect(await screen.findByText("blog page")).toBeInTheDocument();
  });
  it("Should not route to index-page when login is failed", async () => {
    server.use(
      rest.post(`${url}/jwt/create/`, (req, res, ctx) => {
        return res(ctx.status(400));
      })
    );
    const { page } = await getPage({
      route: "/admin-page"
    });
    render(page);
    expect(await screen.findByText("Login")).toBeInTheDocument();
    await userEvent.type(screen.getByPlaceholderText("Username"), "user1");
    await userEvent.type(screen.getByPlaceholderText("Password"), "dummypw");
    await userEvent.click(screen.getByText("Login with JWT"));
    expect(await screen.findByText("Login Error"));
    expect(screen.getByText("Login")).toBeInTheDocument();
    expect(screen.queryByText("blog page")).toBeNull();
  });
  it("Should change to register mode", async () => {
    const { page } = await getPage({
      route: "/admin-page"
    });
    render(page);
    expect(await screen.findByText("Login")).toBeInTheDocument();
    expect(screen.getByText("Login with JWT")).toBeInTheDocument();
    await userEvent.click(screen.getByTestId("mode-change"));
    expect(screen.getByText("Sign up")).toBeInTheDocument();
    expect(screen.getByText("Create new user")).toBeInTheDocument();
  });
  it("Should route to index-page when register and login succeeded", async () => {
    const { page } = await getPage({
      route: "/admin-page"
    });
    render(page);
    expect(await screen.findByText("Login")).toBeInTheDocument();
    await userEvent.click(screen.getByTestId("mode-change"));
    await userEvent.type(screen.getByPlaceholderText("Username"), "user1");
    await userEvent.type(screen.getByPlaceholderText("Password"), "dummypw");
    await userEvent.click(screen.getByText("Create new user"));
    expect(await screen.findByText("blog page")).toBeInTheDocument();
  });
  it("Should not route to index-page when registration is failed", async () => {
    server.use(
      rest.post(`${url}/register/`, (req, res, ctx) => {
        return res(ctx.status(400));
      })
    );
    const { page } = await getPage({
      route: "/admin-page"
    });
    render(page);
    expect(await screen.findByText("Login")).toBeInTheDocument();
    await userEvent.click(screen.getByTestId("mode-change"));
    await userEvent.type(screen.getByPlaceholderText("Username"), "user1");
    await userEvent.type(screen.getByPlaceholderText("Password"), "dummypw");
    await userEvent.click(screen.getByText("Create new user"));
    expect(await screen.findByText("Registration Error"));
    expect(screen.getByText("Sign up")).toBeInTheDocument();
    expect(screen.queryByText("blog page")).toBeNull();
  });
});
