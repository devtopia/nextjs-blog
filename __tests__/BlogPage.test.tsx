import "@testing-library/jest-dom";
import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { getPage, initTestHelpers } from "next-page-tester";

initTestHelpers();

process.env.NEXT_PUBLIC_RESTAPI_URL = "http://127.0.0.1:8000/api";
const url = String(process.env.NEXT_PUBLIC_RESTAPI_URL);

const handlers = [
  rest.get(`${url}/get-blogs/`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([
        {
          id: 1,
          title: "title1",
          content: "content1",
          username: "username1",
          tags: [
            { id: 1, name: "tags1" },
            { id: 2, name: "tags2" }
          ],
          created_at: "2021-01-12 14:59:41"
        },
        {
          id: 2,
          title: "title2",
          content: "content2",
          username: "username2",
          tags: [
            { id: 1, name: "tags1" },
            { id: 2, name: "tags2" }
          ],
          created_at: "2021-01-12 14:59:41"
        }
      ])
    );
  }),
  rest.get(`${url}/get-blogs/1`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        id: 1,
        title: "title1",
        content: "content1",
        username: "username1",
        tags: [
          { id: 1, name: "tag1" },
          { id: 2, name: "tag2" }
        ],
        created_at: "2021-01-12 14:59:41"
      })
    );
  }),
  rest.get(`${url}/get-blogs/2`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        id: 2,
        title: "title2",
        content: "content2",
        username: "username2",
        tags: [
          { id: 1, name: "tag1" },
          { id: 2, name: "tag2" }
        ],
        created_at: "2021-01-12 14:59:41"
      })
    );
  })
];
const server = setupServer(...handlers);
beforeAll(() => {
  server.listen();
});
afterEach(() => {
  server.resetHandlers();
  cleanup();
  document.cookie = "access_token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
});
afterAll(() => {
  server.close();
});
describe("BlogPage Test Cases", () => {
  it("Should route to admin page and route back to blog page", async () => {
    const { page } = await getPage({
      route: "/"
    });
    render(page);
    await userEvent.click(screen.getByTestId("admin-nav"));
    expect(await screen.findByText("Login")).toBeInTheDocument();
    await userEvent.click(screen.getByTestId("blog-nav"));
    expect(await screen.findByText("blog page")).toBeInTheDocument();
  });
  it("Should render delete btn and logout btn when JWT token coockie exist", async () => {
    document.cookie = "access_token=123xyz";
    const { page } = await getPage({
      route: "/"
    });
    render(page);
    expect(await screen.findByText("blog page")).toBeInTheDocument();
    expect(screen.getByTestId("logout-icon")).toBeInTheDocument();
    expect(screen.getByTestId("btn-1")).toBeInTheDocument();
    expect(screen.getByTestId("btn-2")).toBeInTheDocument();
  });
  it("Should not render delete btn and logout btn when no cookie", async () => {
    const { page } = await getPage({
      route: "/"
    });
    render(page);
    expect(await screen.findByText("blog page")).toBeInTheDocument();
    expect(screen.queryByTestId("logout-icon")).toBeNull();
    expect(screen.queryByTestId("btn-1")).toBeNull();
    expect(screen.queryByTestId("btn-2")).toBeNull();
  });
  it("Should render the list of blogs pre-fetched by getStaticProps", async () => {
    const { page } = await getPage({
      route: "/"
    });
    render(page);
    expect(await screen.findByText("blog page")).toBeInTheDocument();
    expect(screen.getByText("title1")).toBeInTheDocument();
    expect(screen.getByText("title2")).toBeInTheDocument();
  });
});

describe("BlogDetailPage Test Cases", () => {
  it("Should render detailed content of ID 1", async () => {
    const { page } = await getPage({
      route: "/posts/1"
    });
    render(page);
    expect(await screen.findByText("title1")).toBeInTheDocument();
    expect(screen.getByText("content1")).toBeInTheDocument();
    expect(screen.getByText("by username1")).toBeInTheDocument();
    expect(screen.getByText("tag1")).toBeInTheDocument();
    expect(screen.getByText("tag2")).toBeInTheDocument();
  });
  it("Should render detailed content of ID 2", async () => {
    const { page } = await getPage({
      route: "/posts/2"
    });
    render(page);
    expect(await screen.findByText("title2")).toBeInTheDocument();
    expect(screen.getByText("content2")).toBeInTheDocument();
    expect(screen.getByText("by username2")).toBeInTheDocument();
    expect(screen.getByText("tag1")).toBeInTheDocument();
    expect(screen.getByText("tag2")).toBeInTheDocument();
  });
  it("Should route back to blog-page from detail page", async () => {
    const { page } = await getPage({
      route: "/posts/2"
    });
    render(page);
    await screen.findByText("title2");
    await userEvent.click(screen.getByTestId("back-blog"));
    expect(await screen.findByText("blog page")).toBeInTheDocument();
  });
});
