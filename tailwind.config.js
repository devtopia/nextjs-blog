/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./pages/**/*.{ts,tsx}", "./components/**/*.{ts,tsx}"],
  darkMode: false,
  theme: {
    extend: {}
  },
  variants: {
    extends: { opacity: ["disabled"] }
  },
  plugins: []
};
