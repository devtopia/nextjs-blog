import axios from "axios";

import { IPost } from "../types/types";

export const getAllPostsData = async (): Promise<IPost[]> => {
  const url = String(process.env.NEXT_PUBLIC_RESTAPI_URL);
  const res = await axios.get(`${url}/get-blogs/`);
  return res.data;
};

export const getPostData = async (id: string): Promise<IPost> => {
  const url = String(process.env.NEXT_PUBLIC_RESTAPI_URL);
  const res = await axios.get(`${url}/get-blogs/${id}`);
  return res.data;
};
